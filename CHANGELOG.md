# Changelog
## [0.6] - 2020-03-16
 - Show dismiss button only on hover (32d1ed52)
 - Make indicator more compact (a84221b5)
 - Add 3.36 version compatibility 
 - Code cleanup
  
## [0.51] - 2020-01-29
 - Fix bug: indicator bubble not always round
 - Fix regression: Fatal error on wayland

## [0.5] - 2020-01-29

 - New option to hide subject when the mail list exceeds specified value
 - Menu redesign
 - Indicator redesign
 - Make avatar size follow mail item size (ie reactive to theme and font size)
 - Make avatar always round
 - Massive code refactoring
 - Fix bug : indicator height don't fit screen (add scrollbar)
 - Code cleaning

## [0.4] - 2020-02-08

 - Fix bug : Dismiss specific mail not working (45a1f3a1)
 - Fix bug : Translation not working (46bdd426)
 - Code cleaning
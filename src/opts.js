/* Bubblemail - GNOME-Shell extension frontend
*
* Copyright 2019 - 2020 Razer <razerraz@free.fr>
* Copyright 2016, 2019 Patrick Ulbrich <zulu99@gmx.net>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*/

/* eslint-disable no-unused-vars */
var AVATAR_ICON_SIZE = 38;
var GETTEXT_DOMAIN = 'bubblemail-gnome-shell';

var KEY = {
  SHOW_AVATARS: 'show-avatars',
  MIN_HIDE_SUBJECT: 'min-hide-subject',
  NEWEST_FIRST: 'newest-first',
  SHOW_DATES: 'show-dates',
  GROUP_BY_ACCOUNT: 'group-by-account',
  ALWAYS_SHOW: 'always-show',
};

var Options = class {
  constructor() {
    this.new_first = false;
    this.show_date = true;
    this.min_hide_subject = 15;
    this.group = false;
    this.always_show = true;
    this.avatar_default = null;
    this.avatars = {};
    this.avatar_size = 38;
  }
};

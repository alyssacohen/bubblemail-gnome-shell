/* Bubblemail - GNOME-Shell extension frontend
*
* Copyright 2019 - 2020 Razer <razerraz@free.fr>
* Copyright 2013 - 2019 Patrick Ulbrich <zulu99@gmx.net>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*/

const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;
const GObject = imports.gi.GObject;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Opts = Me.imports.opts;
const Gettext = imports.gettext.domain(Opts.GETTEXT_DOMAIN);
const _ = Gettext.gettext;

function labeled_box(text) {
  let hbox = new Gtk.Box({orientation: Gtk.Orientation.HORIZONTAL, spacing: 10});
  hbox.pack_start(new Gtk.Label({label: text, xalign: 0}), true, true, 0);
  return hbox;
}

function switch_option(settings, key, text) {
  let hbox = labeled_box(text);
  let switcher = new Gtk.Switch({active: settings.get_boolean(key)});
  settings.bind(key, switcher, 'active', Gio.SettingsBindFlags.DEFAULT);
  hbox.add(switcher);
  return hbox;
}

function init() {  // eslint-disable-line no-unused-vars
  ExtensionUtils.initTranslations();
}

var BubblemailSettingsWidget = GObject.registerClass(
  class BubblemailSettingsWidget extends Gtk.Box {
    _init() {
      super._init({orientation: Gtk.Orientation.VERTICAL, spacing: 16, margin:16});
      let settings = ExtensionUtils.getSettings();
      this.add(switch_option(
        settings, Opts.KEY.NEWEST_FIRST, _('Show newest mails at first')));
      this.add(switch_option(
        settings, Opts.KEY.ALWAYS_SHOW, _('Show the indicator when maillist is empty')));
      let vbox = new Gtk.Box({orientation: Gtk.Orientation.VERTICAL, spacing: 6});
      let hbox = labeled_box(_('Hide mail subjects when mail list exceeds'));
      let spinbutton = Gtk.SpinButton.new_with_range(0, 50, 1);
      spinbutton.set_value(settings.get_int(Opts.KEY.MIN_HIDE_SUBJECT));
      settings.bind(Opts.KEY.MIN_HIDE_SUBJECT, spinbutton, 'value', Gio.SettingsBindFlags.DEFAULT);
      hbox.add(spinbutton);
      vbox.add(hbox);
      vbox.add(new Gtk.Label(
        {label: '<i>' +  _('Subjects will be always visible if set to 0') + '</i>', xalign: 1, 
        use_markup: true}));
      this.add(vbox);        
      this.add(switch_option(
        settings, Opts.KEY.GROUP_BY_ACCOUNT, _('Group mails by account')));
      this.add(switch_option(
        settings, Opts.KEY.SHOW_AVATARS, _('Show avatars')));      
      this.add(switch_option(settings, Opts.KEY.SHOW_DATES, _('Show dates')));
    }
  }
);

function buildPrefsWidget() {  // eslint-disable-line no-unused-vars
  let widget = new BubblemailSettingsWidget();
  widget.show_all();
  return widget;
}

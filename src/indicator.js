/* Bubblemail - GNOME-Shell extension frontend
*
* Copyright 2019 - 2020 Razer <razerraz@free.fr>
* Copyright 2013 - 2019 Patrick Ulbrich <zulu99@gmx.net>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*/

const {Clutter, St, GObject} = imports.gi;
const Gio = imports.gi.Gio;
const Shell = imports.gi.Shell;
const Util = imports.misc.util;
const PopupMenu = imports.ui.popupMenu;
const PanelMenu = imports.ui.panelMenu;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Opts = Me.imports.opts;
const Utils = Me.imports.utils;
const MailItem = Me.imports.mailitem;
const Gettext = imports.gettext.domain(Opts.GETTEXT_DOMAIN);
const _ = Gettext.gettext; 

const SERVICE_MIN_VERSION = 0.29;
const NM_STATE_DISCONNECTED = 20;
const INDICATOR_ICON  = 'mail-unread-symbolic';
const ERROR_NOSERVICE = 1;
const ERROR_SERVICE_STOPPED = 2;
const ERROR_VERSION_MISMATCH = 3;
const ERROR_ACCOUNT = 4;

var MailSubMenu = class extends PopupMenu.PopupSubMenu {
  constructor(actor, arrow) {
    super(actor, arrow);
    this.bypass_close = false;
    this.actor.remove_style_class_name('popup-sub-menu');
  };
  close() {
    // Prevent mail menu to be closed overwriting the close method
    if (this.bypass_close) return;
    PopupMenu.PopupSubMenu.prototype.close.call(this);
  };
};


var MailSubMenuItem = GObject.registerClass(  // eslint-disable-line no-unused-vars
class MailSubMenuItem extends PopupMenu.PopupSubMenuMenuItem {
  
  _init() {
    super._init('', false);
    this.remove_style_class_name('popup-submenu-menu-item');
    this.remove_style_class_name('popup-menu-item');
    this.set_style('font-size: 0%;');
    this._triangle.hide();
    this.menu.destroy();
    this.menu = new MailSubMenu(this, this._triangle);
    this.menu.connect('open-state-changed', this._subMenuOpenStateChanged.bind(this));
    this.connect('destroy', () => this.menu.destroy());
  }
});


var BubblemailIndicator = GObject.registerClass(  // eslint-disable-line no-unused-vars
class BubblemailIndicator extends PanelMenu.Button {

  _init(options, extension) {
    super._init(0.0, null, false);
    this.options = options;
    this.extension = extension;
    this.options.max_width = Math.floor(Utils.get_screen_width()/6);
    this.error_map = [
      [],
      [_('Bubblemail service not found !'), _('Go to download page')],
      [_('Bubblemail service is not running !'), _('Launch settings')],
      [_('Bubblemail service version mismatch, please upgrade !'), _('Go to download page')],
      [_('Account error'), _('Launch settings')]
    ];
    let hbox = new St.BoxLayout({style_class: 'panel-status-menu-box'});
    this.icon = new St.Icon({icon_name: INDICATOR_ICON,  style_class: 'system-status-icon'});
    this.icon_bin = new St.Bin({child: this.icon, x_fill: false, y_fill: false});
    hbox.add(this.icon_bin);
    this.icon_space = new St.Label({text: '', style_class: 'arrow'});
    hbox.add(this.icon_space);
    hbox.add(new PopupMenu.arrowIcon(St.Side.BOTTOM));
    this.icon_lbl= new St.Label({
      text: '0', x_align: Clutter.ActorAlign.CENTER, x_expand: true, y_expand: true,
      y_align: Clutter.ActorAlign.CENTER});
    this.icon_lbl_bin = new St.Bin({
      style_class: 'counter', child: this.icon_lbl,
      layout_manager: new Clutter.BinLayout()});
    this.icon_lbl_bin.connect('style-changed', () => {
      let themeNode = this.icon_lbl_bin.get_theme_node();
      this.icon_lbl_bin.translation_x = themeNode.get_length('-counter-overlap-x');
      this.icon_lbl_bin.translation_y = themeNode.get_length('-counter-overlap-y');
    });
    this.add_actor(hbox);
    this.add_actor(this.icon_lbl_bin);
    // Status menu item
    this.status_menuitem = null;
    // Mail submenu
    this.mail_submenu = new MailSubMenuItem();
    this.menu.addMenuItem(this.mail_submenu);
    this.mail_separator = new PopupMenu.PopupSeparatorMenuItem();
    this.menu.addMenuItem(this.mail_separator );
    this.mail_separator.hide();
    // Dismiss + refresh items
    this.dismiss_all = new PopupMenu.PopupMenuItem(_('Dismiss all'));
    this.dismiss_all.connect('activate', () => {
      this.extension.dismiss_all();
      this.update([]);
    });
    this.menu.addMenuItem(this.dismiss_all);
    this.refresh = new PopupMenu.PopupMenuItem(_('Refresh'));
    this.refresh.connect('activate', () => { this.extension.refresh(); });
    this.menu.addMenuItem(this.refresh);
    // Settings submenu
    this.settings_submenu = new PopupMenu.PopupSubMenuMenuItem(_('Settings'), false);
    let menu_item = new PopupMenu.PopupMenuItem(_('Service Settings'));
    menu_item.connect('activate', () => {this.run_service_settings();});
    this.settings_submenu.menu.addMenuItem(menu_item);
    menu_item = new PopupMenu.PopupMenuItem(_('Extension Settings'));
    menu_item.connect('activate', () => {
      Util.spawn(['gnome-shell-extension-prefs', 'bubblemail@razer.framagit.org']);});
    this.settings_submenu.menu.addMenuItem(menu_item);
    this.menu.addMenuItem(this.settings_submenu);
    this.update([]);
  }

  _onOpenStateChanged(menu, open) {
    super._onOpenStateChanged(menu, open);
    // Trigger mail menu opening here to adapt it's size correctly in a scrollbox
    this.mail_submenu.setSubmenuShown(true);
  }

  vfunc_allocate(box, flags) {
    super.vfunc_allocate(box, flags);
    // get the allocation box of the indicator icon
    let icon_box = this.icon_bin.child.get_allocation_box();
    // create a temporary box for calculating the counter allocation
    let child_box = new Clutter.ActorBox();
    let [unused_w, unused_h, natural_w, natural_h] = this.icon_lbl_bin.get_preferred_size();
    let direction = this.get_text_direction();
    if (direction == Clutter.TextDirection.LTR) {
      // allocate on the right in LTR
      child_box.x1 = icon_box.x2 - natural_w / 2 ;
      child_box.x2 = child_box.x1 + natural_w;
    } else {
      // allocate on the left in RTL
      child_box.x1 = icon_box.x1 - natural_w / 2;
      child_box.x2 = child_box.x1 + natural_w;
    }
    child_box.y1 = icon_box.y2 - natural_h / 2;
    child_box.y2 = child_box.y1 + natural_h;
    this.icon_lbl_bin.allocate(child_box, flags);
  }

  populate_warning() {
    if (!Object.prototype.hasOwnProperty.call(this.extension.core_config, 'poll_interval'))
      return;
    if (this.extension.core_config.poll_interval === '0') {
      this.icon_lbl_bin.visible = true;
      if (!this.icon_lbl_bin.has_style_class_name('warning'))
        this.icon_lbl_bin.add_style_class_name('warning');
      if (!this.extension.mail_map.length) this.icon_lbl.set_text('||');
      this.add_status('media-playback-pause-symbolic', _('Auto refresh is disabled'), 
                      'no-networks-label warning-lbl');
    } else {
      if (this.icon_lbl_bin.has_style_class_name('warning'))
        this.icon_lbl_bin.remove_style_class_name('warning');
    }
  }

  populate_error(error) {
    this.icon_lbl_bin.visible = true;
    this.icon.opacity = 100;
    if (!this.icon_lbl_bin.has_style_class_name('error'))
      this.icon_lbl_bin.add_style_class_name('error');
    this.icon_lbl.set_text('!');
    this.add_status('dialog-error-symbolic', error[0], 'error-lbl', error[1]);
  }

  populate(mail_map) {
    if (!this.error && this.icon_lbl_bin.has_style_class_name('error'))
      this.icon_lbl_bin.remove_style_class_name('error');
    if (mail_map.length) {
      this.mail_submenu.menu.bypass_close = true;
      this.mail_submenu.show();
      this.mail_separator.show();
      this.dismiss_all.show();
      if (this.options.group && mail_map[0]['account'].length) {
        this.add_grouped(mail_map);
      } else {
        this.add(mail_map);
      }
    }
  }

  add(mail_map) {
    for (let mail of mail_map) {
      this.mail_submenu.menu.addMenuItem(
        new MailItem.MailItem(mail, this.options, this.extension));
    }
  }

  add_grouped(mail_map) {
    let grp_mailmap = {};
    for (let mail of mail_map) {
      for (let account of this.extension.account_map) {
        if (mail.account != account.uuid) continue;
        if (!Object.prototype.hasOwnProperty.call(grp_mailmap, account.name))
          grp_mailmap[account.name] = [];
        grp_mailmap[account.name].push(mail);
        break;
      }
    }
    let count = 0;
    for (let account of Object.keys(grp_mailmap)) {
      if (!grp_mailmap[account].length) continue;
      let account_menuitem = new PopupMenu.PopupBaseMenuItem({reactive: true, can_focus: false, 
                                                              activate: false, hover: false});
      let account_label = new St.Label({text: account, 
                                        style_class: 'no-networks-label account-lbl'});
      account_label.clutter_text.single_line_mode= true;
      account_menuitem.add_child(account_label);
      this.mail_submenu.menu.addMenuItem(account_menuitem);
      this.add(grp_mailmap[account]);
      if (count)
        account_label.add_style_class_name('account-space');
      count++;
    }
  }

  run_service_settings() {
    Utils.run_desktop_app(Utils.get_settings_app(this.extension.core_config));
  }

  add_status(icon_name, message, style_class, button_label) {
    if (this.status_menuitem) return;
    this.status_menuitem = new PopupMenu.PopupBaseMenuItem({reactive: false});
    let hbox = new St.BoxLayout(
      {style_class: 'status-box', x_align: Clutter.ActorAlign.CENTER, x_expand: true});
    let icon = new St.Icon({
      icon_name: icon_name, style_class: 'system-status-icon', icon_size: 24
    });
    let label = new St.Label({
      text: message, style_class: style_class, y_align: Clutter.ActorAlign.CENTER,
    });
    hbox.add(icon);
    hbox.add(label);
    if (button_label) {
      let vbox = new St.BoxLayout({vertical: true, style_class: 'status-box'});
      let button = new St.Button({
        reactive: true, can_focus: true, track_hover: true, label:button_label,
        style_class: 'button'
      });
      button.connect('clicked', () => {
        this.menu.close();
        switch (this.error) {
        case ERROR_NOSERVICE:
          Gio.AppInfo.launch_default_for_uri('http://bubblemail.free.fr/', null);
          break;
        case ERROR_SERVICE_STOPPED:
        case ERROR_ACCOUNT:
          this.run_service_settings();
          break;
        default:
          break;
        }
      });
      vbox.add(hbox);
      vbox.add(button, {x_align: St.Align.CENTER});
      this.status_menuitem.add(vbox);
    } 
    else {
      this.status_menuitem.add(hbox);
    }
    this.menu.addMenuItem(this.status_menuitem, 0);
  }

  update(mail_map) {
    if (this.status_menuitem) {
      this.status_menuitem.destroy();
      this.status_menuitem = null;
    };
    this.mail_submenu.menu.bypass_close = false;
    this.mail_submenu.menu.removeAll();
    this.mail_submenu.menu.close();
    this.mail_submenu.hide();
    this.mail_separator.hide();
    this.settings_submenu.hide();
    this.dismiss_all.hide();
    this.refresh.hide();
    this.icon_space.hide();
    if (!this.extension.dbus_map.bubblemail) {
      this.icon_lbl_bin.visible = true;
      this.icon.opacity = 100;
      let settings_app = Utils.get_settings_app(this.extension.core_config);
      if (!settings_app || !Shell.AppSystem.get_default().lookup_app(settings_app)) {
        Utils.ilog('No service found !');
        this.error = ERROR_NOSERVICE;
        this.populate_error(this.error_map[ERROR_NOSERVICE]);
        return;
      }
      this.error = ERROR_SERVICE_STOPPED;
      this.populate_error(this.error_map[ERROR_SERVICE_STOPPED]);
      return;
    }
    if (!Object.prototype.hasOwnProperty.call(this.extension.core_config, 'version')) {
      this.populate_error(this.error_map[ERROR_VERSION_MISMATCH]);
      return;
    }
    let service_version = this.extension.core_config.version.replace('beta', '');
    if (parseFloat(service_version.replace('alpha', '')) < SERVICE_MIN_VERSION) {
      this.populate_error(this.error_map[ERROR_VERSION_MISMATCH]);
      return;
    }
    this.refresh.show();
    this.settings_submenu.show();
    this.error = 0;
    for (let account of this.extension.account_map) {
      if (!account.enable || !account.error.length) continue;
      // Do not show error for insecure connection
      if (Object.prototype.hasOwnProperty.call(account, 'status') && account.status == 4) continue;
      let account_error = [...this.error_map[ERROR_ACCOUNT]];
      account_error[0] = _('Error on account ') + `${account.name}: ${account.error}`;
      this.error = ERROR_ACCOUNT;
      this.populate_error(account_error);
    }
    let label = mail_map.length <= 99 ? mail_map.length.toString() : '...';
    if (label != '0') {
      this.icon_lbl.set_text(label);
    }
    if (mail_map.length > 0) {
      this.icon_space.show();
      this.icon_lbl_bin.visible = true;
      this.icon.opacity = 255;
    } else {
      this.icon_lbl_bin.visible = this.error;
      this.icon.opacity = 130;
    }
    if (this.extension.nm_state == NM_STATE_DISCONNECTED) {
      if (!this.icon_lbl_bin.has_style_class_name('offline'))
        this.icon_lbl_bin.add_style_class_name('offline');
    } else {
      if (this.icon_lbl_bin.has_style_class_name('offline'))
        this.icon_lbl_bin.remove_style_class_name('offline');
    }
    this.populate_warning();
    this.populate(mail_map);
  }
});

/* Bubblemail - GNOME-Shell extension frontend
*
* Copyright 2019 - 2020 Razer <razerraz@free.fr>
* Copyright 2013 - 2019 Patrick Ulbrich <zulu99@gmx.net>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*/

const Main = imports.ui.main;
const Gio = imports.gi.Gio;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Indicator = Me.imports.indicator;
const Opts = Me.imports.opts;
const Utils = Me.imports.utils;

const NM_DBUS_NAME = 'org.freedesktop.NetworkManager';
const NM_DBUS_PATH = '/org/freedesktop/NetworkManager';
const NM_STATE_DISCONNECTED = 20;
const NM_STATE_CONNECTED = 50;
const DBUS_NAME = 'bubblemail.BubblemailService';
const DBUS_PATH = '/bubblemail/BubblemailService';
const CONFIG_UPDATE_SIGNAL = 'ConfigUpdate';
const CONTENT_UPDATE_SIGNAL = 'ContentUpdate';
const GETCONFIG_METHOD = 'GetConfig';
const GETCONTENT_METHOD = 'GetContent';
const REFRESH_METHOD = 'Refresh';
const DISMISS_METHOD = 'Dismiss';
const DISMISSALL_METHOD = 'DismissAll';

const NmIface =
`<node>\
  <interface name="${NM_DBUS_NAME}">\
    <method name="state">\
      <arg type="u" direction="out" />\
    </method>\
    <signal name="StateChanged">\
      <arg type="u" />\
    </signal>\
  </interface>\
</node>`;

const BubblemailIface =
`<node>\
  <interface name="${DBUS_NAME}">\
    <method name="${GETCONTENT_METHOD}">\
      <arg type="aa{ss}" direction="out" />\
    </method>\
    <method name="${GETCONFIG_METHOD}">\
      <arg type="aa{ss}" direction="out" />\
    </method>\
    <method name="${REFRESH_METHOD}" />\
    <method name="${DISMISS_METHOD}">\
      <arg type="s" direction="in" />\
    </method>\
    <method name="${DISMISSALL_METHOD}" />\
    <signal name="${CONFIG_UPDATE_SIGNAL}">\
      <arg type="aa{ss}" />\
    </signal>\
    <signal name="${CONTENT_UPDATE_SIGNAL}">\
      <arg type="aa{ss}" />\
    </signal>\
  </interface>\
</node>`;

const NmDbus = Gio.DBusProxy.makeProxyWrapper(NmIface);
const BubblemailDbus = Gio.DBusProxy.makeProxyWrapper(BubblemailIface);

var BubblemailExtension = class {
  constructor(options) {
    this.mail_map = [];
    this.core_config = [];
    this.account_map = [];
    this.mail_map = [];
    this.options = options;
    this.indicator = null;
    this.dbus_map = {'bubblemail': null, 'networkmanager':null};
    this.watcher_map = {'bubblemail': null, 'networkmanager':null};
    this.signal_map = {'config': null, 'account': null, 'content': null};
    this.nm_state_changed_signal = null;
    this.nm_state = null;
    this.connect_watchers();
    this.create();
  }

  connect_watchers() {
    this.watcher_map.networkmanager = Gio.DBus.system.watch_name(
      NM_DBUS_NAME, Gio.BusNameWatcherFlags.NONE,
      (unused_owner) => {
        this.dbus_map.networkmanager = new NmDbus(Gio.DBus.system, NM_DBUS_NAME, NM_DBUS_PATH);
        this.nm_state_changed_signal = this.dbus_map.networkmanager.connectSignal(
          "StateChanged", (proxy, sender, state) => { this.nm_state_changed(state); }
        );
        this.dbus_map.networkmanager[`stateRemote`]( (state, unused_error) => {
          this.nm_state_changed(state);
        });
      },
      (unused_owner) => {
        this.dbus_map.networkmanager = null;
        this.nm_state_changed_signal = null;
        this.nm_state = null;
      }
    );
    this.watcher_map.bubblemail = Gio.DBus.session.watch_name(
      DBUS_NAME, Gio.BusNameWatcherFlags.NONE,
      (unused_owner) => {
        this.connect_service();
        this.update();
      },
      (unused_owner) => {
        this.dbus_map.bubblemail = null;
        this.update();
      }
    );
  }

  nm_state_changed(state) {
    if (state != NM_STATE_DISCONNECTED && state < NM_STATE_CONNECTED) return;
    this.nm_state = state;
    this.update();
  }

  connect_service() {
    this.dbus_map.bubblemail = new BubblemailDbus(Gio.DBus.session, DBUS_NAME, DBUS_PATH);
    this.signal_map.config = this.dbus_map.bubblemail.connectSignal(
      CONFIG_UPDATE_SIGNAL, (proxy, sender, [config_map]) => { 
        // Utils.ilog('Config update signal');
        this.update_conf(config_map); 
      }
    );
    this.signal_map.content = this.dbus_map.bubblemail.connectSignal(
      CONTENT_UPDATE_SIGNAL, (proxy, sender, [mail_map]) => {
        this.mail_map = mail_map;
        // Utils.ilog('Content update signal, mail_map length: ' + this.mail_map.length);
        this.update();
      }
    );
    this.dbus_map.bubblemail[`${GETCONFIG_METHOD}Remote`]( ([config_map], error) => {
      if (error) return;
      // Utils.ilog('Config remote method response');
      this.update_conf(config_map);
    });
    this.dbus_map.bubblemail[`${GETCONTENT_METHOD}Remote`]( ([mail_map], error) => {
      if (error || (!mail_map.length && !this.options.always_show)) return;
      this.mail_map = mail_map;
      // Utils.ilog('Content remote method response, mail_map length: ' + this.mail_map.length);
      this.update();
    });
  }

  update_conf(config_map) {
    this.account_map = [];
    for (let section of config_map) {
      if (section.section_name == 'core') {
        this.core_config = section;
        if (this.core_config.poll_interval === '0' && !this.indicator) this.create();
      }
      else if (section.section_name.search('Account ') != -1) this.account_map.push(section);
    }
    this.update();
  }

  update() {
    if (this.mail_map == null || this.mail_map.length == 0) {
      this.mail_map = [];
      if (this.indicator != null) {
        this.indicator.update(this.mail_map);
        let show_needed = false;
        for (let bin_class of ['warning', 'error']) {
          if (!this.indicator.icon_lbl_bin.has_style_class_name(bin_class)) continue;
          show_needed = true;
          break;
        }
        for (let account of this.account_map) {
          if (!account.error.length) continue;
          show_needed = true;
          break;
        }
        if (!this.options.always_show && !show_needed) this.indicator.hide();
        else this.indicator.show();
      }
      return;
    }
    this.indicator.show();
    this.indicator.update(this.options.new_first ? this.mail_map.reverse() : this.mail_map);
  }

  create() {
    if (this.indicator) return;
    this.indicator = new Indicator.BubblemailIndicator(this.options, this);
    Main.panel.addToStatusArea('bubblemail-indicator', this.indicator, 0);
  }

  destroy() {
    if (!this.indicator) return;
    this.indicator.destroy();
    this.indicator = null;
  }

  dismiss(uuid) {
    if (!this.dbus_map.bubblemail) return;
    let mail_to_dismiss = null;
    for (let mail of this.mail_map) {
      if (!Object.prototype.hasOwnProperty.call(mail, 'uuid')) continue;
      if (mail.uuid != uuid) continue;
      mail_to_dismiss = mail;
    }
    if (!mail_to_dismiss) return;
    this.dbus_map.bubblemail[`${DISMISS_METHOD}Remote`](uuid, (unused_result, error) => {
      if (error)
        Utils.ilog('DismissRemote() failed: ' + error);
      else
        this.mail_map.splice(this.mail_map.indexOf(mail_to_dismiss), 1);
    });
  }

  dismiss_all() {
    if (!this.dbus_map.bubblemail) return;
    this.dbus_map.bubblemail[`${DISMISSALL_METHOD}Remote`]((result, error) => {
      if (error)
        Utils.ilog('DismissAllRemote() failed: ' + error);
      else
        this.mail_map = [];
    });
  }

  refresh() {
    if (!this.dbus_map.bubblemail) return;
    this.dbus_map.bubblemail[`${REFRESH_METHOD}Remote`]((result, error) => {
      if (error) Utils.ilog('Error: RefreshRemote() failed:' + error);
    });
  }

  dispose() {
    if (this.nm_state_changed_signal && this.dbus_map.networkmanager) {
      this.dbus_map.networkmanager.disconnectSignal(this.nm_state_changed_signal);
    }
    if (this.dbus_map.bubblemail) {
      for (let signal of Object.keys(this.signal_map)) {
        if (!this.signal_map[signal]) continue;
        this.dbus_map.bubblemail.disconnectSignal(this.signal_map[signal]);
        this.signal_map[signal] = null;
      }
    }
    for (let watcher of Object.keys(this.watcher_map)) {
      if (!this.watcher_map[watcher]) continue;
      Gio.DBus.session.unwatch_name(this.watcher_map[watcher]);
      this.watcher_map[watcher] = null;
    }
    this.destroy();
  }
};

function create(settings) {
  let opts = new Opts.Options();
  opts.new_first = settings.get_boolean(Opts.KEY.NEWEST_FIRST);
  opts.show_date = settings.get_boolean(Opts.KEY.SHOW_DATES);
  opts.group = settings.get_boolean(Opts.KEY.GROUP_BY_ACCOUNT);
  opts.always_show  = settings.get_boolean(Opts.KEY.ALWAYS_SHOW);
  opts.show_avatars = settings.get_boolean(Opts.KEY.SHOW_AVATARS);
  opts.min_hide_subject = settings.get_int(Opts.KEY.MIN_HIDE_SUBJECT);
  opts.avatar_size  = Opts.AVATAR_ICON_SIZE;
  return new BubblemailExtension(opts);
}

let extension = null;
let settings = null;
let on_reload = false;

function init() {  // eslint-disable-line no-unused-vars
  ExtensionUtils.initTranslations();
}

function enable() {  // eslint-disable-line no-unused-vars
  settings = ExtensionUtils.getSettings();
  settings.connect('changed', (unused_settings, unused_key) => {
    if (extension != null && !on_reload) {
      extension.dispose();
      extension = create(settings);
    }
  });
  extension = create(settings);
}

function disable() {  // eslint-disable-line no-unused-vars
  on_reload = false;
  if (settings != null) {
    settings.run_dispose();
    settings = null;
  }
  if (extension != null) {
    extension.dispose();
    extension = null;
  }
}
